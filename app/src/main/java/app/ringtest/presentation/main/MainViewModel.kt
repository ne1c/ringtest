package app.ringtest.presentation.main

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import app.ringtest.data.exception.NoNetworkException
import app.ringtest.domain.interactor.PostInteractor
import app.ringtest.models.presentation.PostPresentationModel
import io.reactivex.disposables.CompositeDisposable

class MainViewModel(private val postInteractor: PostInteractor) : ViewModel() {
    val posts = MutableLiveData<ArrayList<PostPresentationModel>>()
    val loadingState = MutableLiveData<Boolean>()
    val paginationLoadingState = MutableLiveData<Boolean>()
    val noNetworkError = MutableLiveData<Boolean>()

    private val disposables = CompositeDisposable()

    fun load() {
        if (loadingState.value == null || posts.value == null) {
            loadingState.value = true

            disposables.add(postInteractor.getTopPosts()
                    .doOnEvent { _, _ ->
                        loadingState.value = false
                    }
                    .subscribe({
                        posts.value = it
                    }, {
                        if (it is NoNetworkException) noNetworkError.value = true
                    }))
        }
    }

    fun loadNextPage() {
        if (postInteractor.postsIsAvailable() &&
                (paginationLoadingState.value == null || paginationLoadingState.value == false)) {
            paginationLoadingState.value = true

            disposables.add(postInteractor.getNextTopPosts()
                    .doOnEvent { _, _ ->
                        paginationLoadingState.value = false
                    }
                    .subscribe({
                        posts.value?.addAll(it)
                        posts.value = posts.value
                    }, {
                        if (it is NoNetworkException) noNetworkError.value = true
                    }))
        }
    }

    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}