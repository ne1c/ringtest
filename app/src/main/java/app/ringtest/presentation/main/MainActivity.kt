package app.ringtest.presentation.main

import android.arch.lifecycle.Observer
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import app.ringtest.R
import app.ringtest.presentation.picture.PictureViewerActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel


class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this)

        recyclerView.adapter = PostsAdapter(object : PostsAdapter.ActionListener {
            override fun openLink(url: String) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            }

            override fun openFullThumbnail(url: String, thumbnailImageView: ImageView) {
                val intent = Intent(this@MainActivity, PictureViewerActivity::class.java)
                intent.putExtra(PictureViewerActivity.IMAGE_URL_KEY, url)
                intent.putExtra(PictureViewerActivity.IMAGE_TRANSITION_NAME_KEY,
                        ViewCompat.getTransitionName(thumbnailImageView))

                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this@MainActivity, thumbnailImageView,
                        ViewCompat.getTransitionName(thumbnailImageView))

                startActivity(intent, options.toBundle())
            }
        })

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                val firstPosition = (recyclerView?.layoutManager as LinearLayoutManager)
                        .findFirstVisibleItemPosition()

                if (firstPosition >= recyclerView.adapter.itemCount - 5) {
                    viewModel.loadNextPage()
                }
            }
        })

        viewModel.loadingState.observe(this, Observer {
            progressBar.visibility = if (checkNotNull(it)) {
                View.VISIBLE
            } else {
                View.GONE
            }
        })

        viewModel.paginationLoadingState.observe(this, Observer {
            (recyclerView.adapter as PostsAdapter).showProgress(it!!)
        })

        viewModel.posts.observe(this, Observer {
            recyclerView.visibility = View.VISIBLE
            (recyclerView.adapter as PostsAdapter).setData(it!!)
        })

        viewModel.noNetworkError.observe(this, Observer {
            Toast.makeText(this, "No connection to network", Toast.LENGTH_SHORT).show()
        })

        viewModel.load()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState?.putParcelable("recycler", recyclerView.layoutManager.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        recyclerView.layoutManager.onRestoreInstanceState(savedInstanceState?.getParcelable("recycler"))
    }
}
