package app.ringtest.presentation.main

import android.support.v4.view.ViewCompat
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import app.ringtest.R
import app.ringtest.models.presentation.PostPresentationModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_post.view.*

class PostsAdapter(private val actionListener: ActionListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ITEM_VIEW_TYPE = 0
    private val PROGRESS_VIEW_TYPE = 1

    private var showProgress = false
    private val data = arrayListOf<PostPresentationModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_VIEW_TYPE) {
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false))
        } else {
            val progressBar = ProgressBar(parent.context)
            progressBar.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)

            ProgressViewHolder(progressBar)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (showProgress && position == data.size) {
            PROGRESS_VIEW_TYPE
        } else {
            ITEM_VIEW_TYPE
        }
    }

    // (data.size + 1) == last position for ProgressBar
    override fun getItemCount(): Int = if (showProgress) data.size + 1 else data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE) {
            (holder as ItemViewHolder).bind(data[position])
        }
    }

    fun setData(newData: ArrayList<PostPresentationModel>) {
        val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return data[oldItemPosition].id == newData[newItemPosition].id
            }

            override fun getOldListSize(): Int = data.size

            override fun getNewListSize(): Int = newData.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return data[oldItemPosition].id == newData[newItemPosition].id
            }
        })

        data.clear()
        data.addAll(newData)

        result.dispatchUpdatesTo(this)
    }

    fun showProgress(show: Boolean) {
        showProgress = show

        if (showProgress) {
            notifyItemInserted(data.size)
        } else {
            notifyItemRemoved(data.size + 1)
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: PostPresentationModel) {
            with(itemView) {
                authorTextView.text = model.author
                titleTextView.text = model.title
                createdTextView.text = context.resources.getQuantityString(app.ringtest.R.plurals.hours, model.hours, model.hours)
                countCommentsTextView.text = context.resources.getQuantityString(app.ringtest.R.plurals.comments, model.countComments, model.countComments)

                if (!model.isImage) {
                    thumbnailImageView.visibility = View.GONE
                    openLinkButton.visibility = View.VISIBLE

                    openLinkButton.setOnClickListener {
                        actionListener.openLink(model.fullUrl)
                    }
                } else {
                    thumbnailImageView.visibility = View.VISIBLE
                    openLinkButton.visibility = View.GONE

                    thumbnailImageView.layoutParams.height = model.thumbnailSize.height
                    thumbnailImageView.layoutParams.width = model.thumbnailSize.width
                    Glide.with(this)
                            .load(model.thumbnail)
                            .into(thumbnailImageView)

                    thumbnailImageView.setOnClickListener {
                        actionListener.openFullThumbnail(model.fullUrl, thumbnailImageView)
                    }
                }

                ViewCompat.setTransitionName(thumbnailImageView, model.id)
            }
        }
    }

    class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface ActionListener {
        fun openFullThumbnail(url: String, thumbnailImageView: ImageView)

        fun openLink(url: String)
    }
}