package app.ringtest.di

import android.content.Context
import android.net.ConnectivityManager
import app.ringtest.BuildConfig
import app.ringtest.android.NetworkStateImpl
import app.ringtest.data.network.api.RedditApi
import app.ringtest.data.network.source.NetworkDataSource
import app.ringtest.domain.interactor.PostInteractor
import app.ringtest.domain.repository.PostRepository
import app.ringtest.domain.repository.PostRepositoryImpl
import app.ringtest.domain.util.ExecutorService
import app.ringtest.domain.util.ExecutorServiceImpl
import app.ringtest.domain.util.NetworkState
import app.ringtest.presentation.main.MainViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = applicationContext {
    bean { NetworkDataSource(get()) }
    bean {
        val okHttpClient = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(HttpLoggingInterceptor()
                    .apply { level = HttpLoggingInterceptor.Level.BODY })
        }

        Retrofit.Builder()
                .client(okHttpClient.build())
                .baseUrl("https://reddit.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(RedditApi::class.java)
    }
    bean {
        NetworkStateImpl(androidApplication()
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager) as NetworkState
    }
}

val executorService = applicationContext {
    bean { ExecutorServiceImpl() as ExecutorService }
}

val mainModule = applicationContext {
    viewModel { MainViewModel(get()) }
    bean { PostInteractor(get(), get(), get()) }
    bean { PostRepositoryImpl(get()) as PostRepository }
}