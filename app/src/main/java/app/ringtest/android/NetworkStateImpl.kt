package app.ringtest.android

import android.net.ConnectivityManager
import app.ringtest.domain.util.NetworkState

class NetworkStateImpl(private val connectivityManager: ConnectivityManager) : NetworkState {
    override fun isConnected(): Boolean {
        return connectivityManager.activeNetworkInfo?.isConnectedOrConnecting ?: false
    }
}