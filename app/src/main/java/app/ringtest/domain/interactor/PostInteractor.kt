package app.ringtest.domain.interactor

import app.ringtest.data.exception.NoNetworkException
import app.ringtest.domain.repository.PostRepository
import app.ringtest.domain.util.ExecutorService
import app.ringtest.domain.util.NetworkState
import app.ringtest.models.presentation.PostPresentationModel
import io.reactivex.Single

class PostInteractor(private val postRepository: PostRepository,
                     private val executorService: ExecutorService,
                     private val networkState: NetworkState) {
    private val limit = 10
    private var afterKey: String? = null
    private var loaded: Int = 0

    fun getTopPosts(): Single<ArrayList<PostPresentationModel>> {
        if (!networkState.isConnected()) return Single.error(NoNetworkException())

        return postRepository.getTopPosts(limit, null)
                .subscribeOn(executorService.backgroundThread())
                .map {
                    afterKey = it.data?.afterKey

                    return@map it.data?.posts?.mapTo(arrayListOf()) {
                        PostPresentationModel.map(it.postModel!!)
                    } ?: arrayListOf<PostPresentationModel>()
                }
                .doOnSuccess {
                    loaded += 10
                }
                .observeOn(executorService.uiThread())
    }

    fun getNextTopPosts(): Single<ArrayList<PostPresentationModel>> {
        if (!networkState.isConnected()) return Single.error(NoNetworkException())

        return postRepository.getTopPosts(limit, afterKey)
                .subscribeOn(executorService.backgroundThread())
                .map {
                    afterKey = it.data?.afterKey

                    return@map it.data?.posts?.mapTo(arrayListOf()) {
                        PostPresentationModel.map(it.postModel!!)
                    } ?: arrayListOf<PostPresentationModel>()
                }
                .doOnSuccess {
                    loaded += 10
                }
                .observeOn(executorService.uiThread())
    }

    fun postsIsAvailable() = loaded < 50
}