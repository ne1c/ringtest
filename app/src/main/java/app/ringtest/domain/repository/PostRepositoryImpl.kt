package app.ringtest.domain.repository

import app.ringtest.data.network.response.TopPostsResponse
import app.ringtest.data.network.source.NetworkDataSource
import io.reactivex.Single

class PostRepositoryImpl(private val networkDataSource: NetworkDataSource): PostRepository {
    override fun getTopPosts(limit: Int, afterKey: String?): Single<TopPostsResponse> {
        return networkDataSource.getTopPosts(limit, afterKey)
    }
}