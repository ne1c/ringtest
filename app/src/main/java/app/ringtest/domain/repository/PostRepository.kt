package app.ringtest.domain.repository

import app.ringtest.data.network.response.TopPostsResponse
import io.reactivex.Single

interface PostRepository {
    fun getTopPosts(limit: Int, afterKey: String?): Single<TopPostsResponse>
}