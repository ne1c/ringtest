package app.ringtest.domain.util

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ExecutorServiceImpl : ExecutorService{
    override fun backgroundThread(): Scheduler {
        return Schedulers.io()
    }

    override fun uiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}