package app.ringtest.domain.util

import io.reactivex.Scheduler

interface ExecutorService {
    fun backgroundThread(): Scheduler

    fun uiThread(): Scheduler
}