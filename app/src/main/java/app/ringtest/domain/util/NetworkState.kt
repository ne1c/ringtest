package app.ringtest.domain.util

interface NetworkState {
    fun isConnected(): Boolean
}