package app.ringtest.data.exception

class NoNetworkException : Exception("No connection to network!")