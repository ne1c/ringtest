package app.ringtest.data.network.source

import app.ringtest.data.network.api.RedditApi
import app.ringtest.data.network.response.TopPostsResponse
import io.reactivex.Single

class NetworkDataSource(private val redditApi: RedditApi) {
    fun getTopPosts(limit: Int, afterKey: String?): Single<TopPostsResponse> = redditApi.getTopPosts(limit, afterKey)
}