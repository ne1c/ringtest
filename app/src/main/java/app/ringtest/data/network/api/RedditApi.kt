package app.ringtest.data.network.api

import app.ringtest.data.network.response.TopPostsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RedditApi {
    @GET("/top.json")
    fun getTopPosts(@Query("limit") limit: Int,
                    @Query("after") afterKey: String?): Single<TopPostsResponse>
}