package app.ringtest.data.network.response

import app.ringtest.models.network.PostModel
import com.google.gson.annotations.SerializedName

class TopPostsResponse {
    @SerializedName("data")
    var data: Data? = null
}

class Data {
    @SerializedName("after")
    var afterKey: String? = null

    @SerializedName("children")
    var posts: ArrayList<Child>? = null
}

class Child {
    @SerializedName("data")
    var postModel: PostModel? = null
}