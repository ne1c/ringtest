package app.ringtest

import android.app.Application
import app.ringtest.di.executorService
import app.ringtest.di.mainModule
import app.ringtest.di.networkModule
import org.koin.android.ext.android.startKoin

class RingApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(networkModule, executorService, mainModule))
    }
}