package app.ringtest.models.network

import com.google.gson.annotations.SerializedName

data class PostModel(
        @SerializedName("name")
        val id: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("author")
        val author: String,
        @SerializedName("thumbnail")
        val thumbnail: String,
        @SerializedName("url")
        val fullSizeThumbnail: String,
        @SerializedName("num_comments")
        val countComments: Int,
        @SerializedName("created_utc")
        val createdUtc: Long,
        @SerializedName("thumbnail_height")
        val thumbnailHeight: Int,
        @SerializedName("thumbnail_width")
        val thumbnailWidth: Int)