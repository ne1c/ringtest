package app.ringtest.models.presentation

import android.os.Parcel
import android.os.Parcelable
import android.util.Size
import app.ringtest.models.network.PostModel
import java.util.Date

data class PostPresentationModel(
        val id: String,
        val title: String,
        val author: String,
        val thumbnail: String,
        val fullUrl: String,
        val countComments: Int,
        val hours: Int,
        val thumbnailSize: Size,
        val isImage: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            Size(parcel.readInt(), parcel.readInt()),
            parcel.readByte() != 0.toByte())

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<PostPresentationModel> {
            override fun createFromParcel(parcel: Parcel): PostPresentationModel = PostPresentationModel(parcel)

            override fun newArray(size: Int): Array<PostPresentationModel?> = arrayOfNulls(size)
        }

        fun map(model: PostModel): PostPresentationModel {
            val hours = (Date().time - model.createdUtc * 1000) / 1000 / 60 / 60
            var isImage = false

            val lastDotIndex = model.fullSizeThumbnail?.lastIndexOf(".") ?: -1
            if (lastDotIndex > 0) {
                val extension = model.fullSizeThumbnail?.substring(lastDotIndex + 1)
                isImage = extension == "jpg" || extension == "png"
            }

            return PostPresentationModel(model.id, model.title, model.author, model.thumbnail,
                    model.fullSizeThumbnail, model.countComments, hours.toInt(),
                    Size(model.thumbnailWidth, model.thumbnailHeight), isImage)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(author)
        parcel.writeString(thumbnail)
        parcel.writeString(fullUrl)
        parcel.writeInt(countComments)
        parcel.writeInt(hours)
        parcel.writeInt(thumbnailSize.width)
        parcel.writeInt(thumbnailSize.height)
        parcel.writeByte(if (isImage) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }
}