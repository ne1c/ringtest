package app.ringtest.utils

import app.ringtest.domain.util.NetworkState

class TestNetworkState(private val isConnected: Boolean) : NetworkState {
    override fun isConnected(): Boolean = isConnected
}