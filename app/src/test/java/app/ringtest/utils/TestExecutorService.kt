package app.ringtest.utils

import app.ringtest.domain.util.ExecutorService
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestExecutorService : ExecutorService {
    override fun backgroundThread(): Scheduler = Schedulers.trampoline()

    override fun uiThread(): Scheduler = Schedulers.trampoline()
}