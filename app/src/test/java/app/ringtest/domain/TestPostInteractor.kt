package app.ringtest.domain

import app.ringtest.data.exception.NoNetworkException
import app.ringtest.data.network.response.Child
import app.ringtest.data.network.response.Data
import app.ringtest.data.network.response.TopPostsResponse
import app.ringtest.domain.interactor.PostInteractor
import app.ringtest.domain.repository.PostRepository
import app.ringtest.models.network.PostModel
import app.ringtest.models.presentation.PostPresentationModel
import app.ringtest.utils.TestExecutorService
import app.ringtest.utils.TestNetworkState
import io.reactivex.Single
import io.reactivex.functions.Predicate
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations


class TestPostInteractor {
    @Mock
    private var postRepository: PostRepository? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getTopPosts_success() {
        val interactor = PostInteractor(postRepository!!, TestExecutorService(), TestNetworkState(true))

        val response = generateResponse()

        `when`(postRepository!!.getTopPosts(10, null)).thenReturn(Single.just(response))

        val observer = TestObserver<ArrayList<PostPresentationModel>>()
        interactor.getTopPosts().subscribe(observer)

        observer.assertNoErrors()
        observer.onComplete()

        observer.assertValueCount(1)
        observer.assertValue(Predicate {
            for (i in 0 until 10) {
                if (it[i].id != "testid_$i") return@Predicate false
            }

            return@Predicate true
        })

        assertThat(interactor.postsIsAvailable(), equalTo(true))
    }

    @Test
    fun getTopPosts_failure_noNetwork() {
        val interactor = PostInteractor(postRepository!!, TestExecutorService(), TestNetworkState(false))

        val observer = TestObserver<ArrayList<PostPresentationModel>>()
        interactor.getTopPosts().subscribe(observer)

        observer.assertError(NoNetworkException::class.java)
        observer.onComplete()
    }

    @Test
    fun getNextTopPosts_failure_noNetwork() {
        val interactor = PostInteractor(postRepository!!, TestExecutorService(), TestNetworkState(false))

        val observer = TestObserver<ArrayList<PostPresentationModel>>()
        interactor.getNextTopPosts().subscribe(observer)

        observer.assertError(NoNetworkException::class.java)
        observer.onComplete()
    }

    @Test
    fun getNextTopPosts_success() {
        val interactor = PostInteractor(postRepository!!, TestExecutorService(), TestNetworkState(true))

        val startFrom = 10
        val response = generateResponse(startFrom)

        `when`(postRepository!!.getTopPosts(10, null)).thenReturn(Single.just(response))
        `when`(postRepository!!.getTopPosts(10, "some_key")).thenReturn(Single.just(response))

        val observer = TestObserver<ArrayList<PostPresentationModel>>()

        interactor.getTopPosts().subscribe()
        interactor.getNextTopPosts().subscribe(observer)

        observer.assertNoErrors()
        observer.onComplete()

        observer.assertValueCount(1)
        observer.assertValue(Predicate {
            for (i in 10 until 20) {
                if (it[i - startFrom].id != "testid_$i") return@Predicate false
            }

            return@Predicate true
        })

        assertThat(interactor.postsIsAvailable(), equalTo(true))
    }

    @Test
    fun postsIsAvailable_moreThen50_false() {
        val interactor = PostInteractor(postRepository!!, TestExecutorService(), TestNetworkState(true))

        val response = generateResponse()

        `when`(postRepository!!.getTopPosts(10, null)).thenReturn(Single.just(response))
        `when`(postRepository!!.getTopPosts(10, "some_key")).thenReturn(Single.just(response))

        interactor.getTopPosts().subscribe()

        for (i in 0 until 4) {
            interactor.getNextTopPosts().subscribe()
        }

        assertThat(interactor.postsIsAvailable(), equalTo(false))
    }

    private fun generateTestData(startFrom: Int): ArrayList<Child> {
        val list = arrayListOf<Child>()

        for (i in startFrom until (startFrom + 10)) {
            list.add(Child().apply {
                postModel = PostModel("testid_$i", "",
                        "",
                        "",
                        "",
                        0,
                        0,
                        0,
                        0)
            })
        }

        return list
    }

    private fun generateResponse(startFrom: Int = 0): TopPostsResponse {
        return TopPostsResponse().apply {
            data = Data().apply {
                afterKey = "some_key"
                posts = generateTestData(startFrom)
            }
        }
    }
}