package app.ringtest.presentation

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import app.ringtest.data.network.response.TopPostsResponse
import app.ringtest.domain.interactor.PostInteractor
import app.ringtest.domain.repository.PostRepository
import app.ringtest.models.presentation.PostPresentationModel
import app.ringtest.presentation.main.MainViewModel
import app.ringtest.utils.TestExecutorService
import app.ringtest.utils.TestNetworkState
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class TestMainViewModel {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private var postRepository: PostRepository? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun load_success() {
        val postInteractor = PostInteractor(postRepository!!, TestExecutorService(),
                TestNetworkState(true))
        val mockResponse = mock(TopPostsResponse::class.java)

        `when`(postRepository!!.getTopPosts(10, null))
                .thenReturn(Single.just(mockResponse!!))

        val viewModel = MainViewModel(postInteractor)
        val loadingObserver = mock(Observer::class.java) as Observer<Boolean>
        val postsObserver = mock(Observer::class.java) as Observer<ArrayList<PostPresentationModel>>
        val noNetworkObserver = mock(Observer::class.java) as Observer<Boolean>

        viewModel.loadingState.observeForever(loadingObserver)
        viewModel.posts.observeForever(postsObserver)
        viewModel.noNetworkError.observeForever(noNetworkObserver)

        viewModel.load()

        verify(loadingObserver).onChanged(true)
        verify(loadingObserver).onChanged(false)

        verify(noNetworkObserver, never()).onChanged(ArgumentMatchers.anyBoolean())

        verify(postsObserver).onChanged(ArgumentMatchers.any())
    }

    @Test
    fun load_failure_noNetwork() {
        val postInteractor = PostInteractor(postRepository!!, TestExecutorService(),
                TestNetworkState(false))
        val mockResponse = mock(TopPostsResponse::class.java)

        `when`(postRepository!!.getTopPosts(10, null))
                .thenReturn(Single.just(mockResponse!!))

        val viewModel = MainViewModel(postInteractor)
        val loadingObserver = mock(Observer::class.java) as Observer<Boolean>
        val postsObserver = mock(Observer::class.java) as Observer<ArrayList<PostPresentationModel>>
        val noNetworkObserver = mock(Observer::class.java) as Observer<Boolean>

        viewModel.loadingState.observeForever(loadingObserver)
        viewModel.posts.observeForever(postsObserver)
        viewModel.noNetworkError.observeForever(noNetworkObserver)

        viewModel.load()

        verify(postsObserver, never()).onChanged(ArgumentMatchers.any())

        verify(loadingObserver).onChanged(true)
        verify(loadingObserver).onChanged(false)
        verify(noNetworkObserver).onChanged(true)
    }

    @Test
    fun loadNextPage_success() {
        val postInteractor = PostInteractor(postRepository!!, TestExecutorService(),
                TestNetworkState(true))
        val mockResponse = mock(TopPostsResponse::class.java)

        `when`(postRepository!!.getTopPosts(10, null))
                .thenReturn(Single.just(mockResponse!!))

        val viewModel = MainViewModel(postInteractor)
        val loadingObserver = mock(Observer::class.java) as Observer<Boolean>
        val loadingPaginationObserver = mock(Observer::class.java) as Observer<Boolean>
        val postsObserver = mock(Observer::class.java) as Observer<ArrayList<PostPresentationModel>>
        val noNetworkObserver = mock(Observer::class.java) as Observer<Boolean>

        viewModel.loadingState.observeForever(loadingObserver)
        viewModel.posts.observeForever(postsObserver)
        viewModel.noNetworkError.observeForever(noNetworkObserver)
        viewModel.paginationLoadingState.observeForever(loadingPaginationObserver)

        viewModel.load()
        viewModel.loadNextPage()

        verify(loadingObserver).onChanged(true)
        verify(loadingObserver).onChanged(false)

        verify(loadingPaginationObserver).onChanged(true)
        verify(loadingPaginationObserver).onChanged(false)

        verify(noNetworkObserver, never()).onChanged(ArgumentMatchers.anyBoolean())

        verify(postsObserver, times(2)).onChanged(ArgumentMatchers.any())
    }
}